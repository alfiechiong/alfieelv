import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './components/App.vue'
const URLTYPE = "json";
Vue.use(VueResource)
Vue.use(URLTYPE)

Vue.http.options.root = 'http://localhost:8000'

const app = new Vue({
    el: '#app',
    render: h => h(App)
});
