<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return product::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, product $cur_product)
    {
        if ($request->isMethod('put')) {
            $product = $cur_product->findOrFail($request->product_id);
        } else {
            $product = new product();
        }
        
        $product->product_name = $request->input('product_name');
        $product->quantity = $request->input('quantity');
        $product->price = $request->input('price');

        

        try {
            if ($product->save()) {
                return $product;
            }
        } catch (Exception $exception) {
                return ["error"=>$exception->errorInfo];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\churches  $productes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = product::findOrFail($id);

        return $product;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\churches  $productes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (product::destroy($id)) {
            return ['success'=>"product deleted"];
        }
    }
}
