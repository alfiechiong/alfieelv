<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class JsonController extends Controller
{
    public function index()
    {
        $path = public_path('JSON.json');
        $json = File::get($path);
        return $json;
    }

    public function show($id)
    {
        $path = public_path('JSON.json');
        $json = File::get($path);
        $json = json_decode($json, true);

        $data = $this->searchArr($json, 'id', $id);

        return json_encode($data[0]);
    }

    public function store(Request $request)
    {
        $data = json_decode($this->index(), true);

        $product = ["id" =>uniqid(),
                    "product_name" => $request->product_name,
                    "quantity" => $request->quantity,
                    "price" => $request->price,
                    "dateCreated" => date("Y-m-d h:i:sa")
                    ];

        array_push($data, $product);

        $data = json_encode($data);
        
        $JSON = 'JSON.json';
            
        File::put(public_path($JSON), $data);

        return $this->index();
    }

    public function update(Request $request)
    {
        $data = json_decode($this->index(), true);

        $product = $this->searchArr($data, 'id', $request->product_id);

        $newData = $this->removeArr($request->product_id);

        $newproduct = ["id" =>$request->product_id,
                    "product_name" => $request->product_name,
                    "quantity" => $request->quantity,
                    "price" => $request->price,
                    "dateCreated" => $product[0]['dateCreated'],
                    "dateUpdated" => date("Y-m-d h:i:sa")
                    ];

        array_push($newData, $newproduct);

        $newData = json_encode($newData);
        
        $JSON = 'JSON.json';
            
        File::put(public_path($JSON), $newData);

        return $this->index();
    }

    public function destroy($id)
    {

        $data =  $this->removeArr($id);

        $JSON = 'JSON.json';
            
        File::put(public_path($JSON), json_encode($data));

        return ["msg"=>"successfully deleted"];
    }

    private function searchArr($arr, $key, $val)
    {
            $results = [];

        if (is_array($arr)) {
            if (isset($arr[$key]) && $arr[$key] == $val) {
                    $results[] = $arr;
            }

            foreach ($arr as $subarray) {
                    $results = array_merge($results, $this->searchArr($subarray, $key, $val));
            }
        }
        return $results;
    }

    private function removeArr($id)
    {
        $arr = json_decode($this->index(), true);
        $prod = $this->searchArr($arr, 'id', $id);
        foreach ($arr as $key => $val) {
            foreach ($prod as $prodArr) {
                if ($val['id'] == $prodArr['id']) {
                    unset($arr[$key]);
                    break;
                }
            }
        }
        return $arr;
    }
}
