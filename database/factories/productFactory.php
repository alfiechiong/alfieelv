<?php

use Faker\Generator as Faker;

$factory->define(App\product::class, function (Faker $faker) {
    return [
        'product_name' => $faker->name,
        'quantity' => rand(0, 100),
        'price' => rand(0.1, 100),
    ];
});
