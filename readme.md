===============================================================
================ IF YOU WANT USE JSON FILE=====================
===============================================================

requirements:
    php
    npm 
    composer

STEPS:

run (inside the project folder)
1: npm install | composer install 
2: php artisan serve | npm run dev
3: navigate localhost:8000

===============================================================
================ IF YOU WANT USE THE DATABASE =================
===============================================================

requirements:
    mysql
    php
    npm 
    composer

Steps:

1: create database to use : "inside mysql / mariadb " ("create database DBNAME")

    setup db env 
    -  set db name and db user/pass
    -   DB_DATABASE=`DBNAME`
        DB_USERNAME=`DBUSER`
        DB_PASSWORD=`DBPASSWORD`

run (inside the project folder)
2: composer install
3: npm install
5: go to: rootfolder/resources/assets/components/products.vue and go to line 51 change the value of url to "product"
6: go to: rootfolder/resources/assets/components/create.vue and go to line 47 change the value of url to "product"
7: paste this on terminal:
        php artisan migrate | php artisan db:seed --class=ProductsTableSeeder | npm run dev | php artisan serve
8: navigate: localhost:8000

===============================================================
================ DESCRIPTIONS =================================
===============================================================

BACKEND LARAVEL
FRONTEND VUEJS / JQUERY / BOOTSTRAP
ROOT URL : localhost:8000

AUTHOR: ALFIE CHIONG







